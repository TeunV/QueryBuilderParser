<?php

namespace timgws;

use \Carbon\Carbon;
use \stdClass;
use \Illuminate\Database\Query\Builder;
use \timgws\QBParseException;

class CollectionQueryParser
{
    use QBPFunctions;

    protected $fields;

    /**
     * @param array $fields a list of all the fields that are allowed to be filtered by the QueryBuilder
     */
    public function __construct(array $fields = null)
    {
        $this->fields = $fields;
    }

    /**
     * QueryBuilderParser's parse function!
     *
     * Build a query based on JSON that has been passed into the function, onto the builder passed into the function.
     *
     * @param $json
     * @param Builder $querybuilder
     *
     * @throws QBParseException
     *
     * @return Builder
     */
    public function parse($json, Collection $collection)
    {
        // do a JSON decode (throws exceptions if there is a JSON error...)
        $query = $this->decodeJSON($json);

        // This can happen if the querybuilder had no rules...
        if (!isset($query->rules) || !is_array($query->rules)) {
            return $collection;
        }

        // This shouldn't ever cause an issue, but may as well not go through the rules.
        if (count($query->rules) < 1) {
            return $collection;
        }

        return $this->loopThroughRules($query->rules, $collection, $query->condition)->get();
    }

    /**
     * Called by parse, loops through all the rules to find out if nested or not.
     *
     * @param array $rules
     * @param Builder $querybuilder
     * @param string $queryCondition
     *
     * @throws QBParseException
     *
     * @return Builder
     */
    protected function loopThroughRules(array $rules, Collection $collection, $queryCondition = 'AND')
    {
        foreach ($rules as $rule) {
            $collection = $this->applyFilter($collection, $rule, $queryCondition);
        }

        return $collection;
    }

    protected function applyFilter(Collection $collection, $rule){
        $operator = $this->operator_php[$rule]['operator'];

        if($operator == "IN"){
            return $collection->whereIn($rule->value);
        }
        else if($operator == "NOT IN"){
            return $collection->whereNotIn($rule->value);
        }
        else{
            return $collection->where($rule->field, $operator, $rule->value);
        }
        
    }
}
