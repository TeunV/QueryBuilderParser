<?php
namespace Suresh\Calc;
use Illuminate\Support\ServiceProvider;

class CollectionFilterServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/routes.php';
    }
/**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}